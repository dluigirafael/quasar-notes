export function modelDrawer(state) {
  state.drawer = !state.drawer;
}
export function hideHeader(state) {
  state.header = false;
}
export function showHeader(state) {
  state.header = true;
}
export function hideDrawer(state) {
  state.drawer = false;
}
export function sendNewNote(state) {
  //prevent adding empty note
  if (state.newNote.title != "")
    state.notes.push({
      title: state.newNote.title,
      data: state.newNote.data,
      marked: state.newNote.marked,
    });
  // state.newNote.title = "";
  // state.newNote.data = "";
  // state.newNote.marked = false;
}
export function cleanNewNote(state) {
  state.newNote.title = "";
  state.newNote.data = "";
  state.newNote.marked = false;
}

export function rmNote(state, payload) {
  state.notes.splice(payload, 1);
}

export function markNote(state, payload) {
  state.notes[payload].marked = true;
}

export function newNoteTitle(state, payload) {
  state.newNote.title = payload;
}
export function newNoteData(state, payload) {
  state.newNote.data = payload;
}
export function newNoteMark(state, payload) {
  state.newNote.marked = payload;
}

export function unMark(state, payload) {
  state.notes[payload].marked = false;
}
export function tabTo(state, payload) {
  state.tab = payload;
}
export function wasOnMark(state, payload) {
  state.wasOnMark = payload;
}
