/*
export function someAction (context) {
}
*/
export function asyncSendNote(context) {
  context.commit("sendNewNote");
  context.commit("cleanNewNote");
}
export function asyncMarkNote(context, payload) {
  context.commit("markNote", payload);
}
export function asyncUnMark(context, payload) {
  context.commit("unMark", payload);
}
