export default function() {
  return {
    notes: [],
    newNote: {
      title: "",
      data: "",
      marked: false,
    },
    tab: "notes",
    drawer: false,
    header: true,
    wasOnMark: false,
  };
}
